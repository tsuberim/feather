const Person = entity({
    name: 'Person',
    fields: object({
        name: str,
        age: nullable(int)
    })
})

module.exports = {
    queries: defaultQueries(Person),
    actions: defaultActions(Person)
}
import { dotCase } from "change-case";

export function mapObject<A,B>(obj: {[key: string]: A}, fn: (a: A, key: string) => B): {[key: string]: B} {
    return Object.entries(obj).reduce((acc, [key, val]) => ({...acc, [key]: fn(val, key)}), {})
}

export function toObject<A>(arr: A[], keyfn: (a: A) => string): {[key: string]: A}{
    return arr.reduce((acc, val) => ({...acc, [keyfn(val)]: val}),{});
}

export async function sleep(ms: number) {
    return new Promise(res => setTimeout(res, ms));
}

export function groupBy<A>(items: ReadonlyArray<A>, keyFn: (a: A) => string): {[key: string]: A[]} {
    const groups = {};
    for(const item of items) {
        const key = keyFn(item);
        groups[key] = groups[key] || []
        groups[key].push(item);
    }
    return groups;
}

export function assert(condition: boolean, message: string, path?: string) {
    if (!condition) {
        throw new Error(path ? `${message} at: '${dotCase(path)}'` : message);
    }
}
import { GraphQLBoolean, GraphQLEnumType, GraphQLFieldResolver, GraphQLFloat, GraphQLID, GraphQLInputObjectType, GraphQLInputType, GraphQLInt, GraphQLList, GraphQLNonNull, GraphQLObjectType, GraphQLOutputType, GraphQLScalarType, GraphQLSchema, GraphQLString } from "graphql";
import { createValidator, GeoPoint, MethodDef, ProjectDef, TypeDef, walk } from "./defs";
import { mapObject, toObject } from "./utils";
import { pascalCase } from 'change-case';
import { UserInputError } from "apollo-server-express";
import { DBHandle } from "./db";
import asyncify from 'callback-to-async-iterator';
import { createLogger } from "./logger";
import uniqid from 'uniqid';

const logger = createLogger('graphql')

export const GraphQLDateTime = new GraphQLScalarType({
    name: 'DateTime',
    parseValue(value) {
        return new Date(value);
    },
    serialize(value) {
        return value.toISOString()
    },
})

export const GraphQLGeoPoint = new GraphQLScalarType({
    name: 'GeoPoint',
    parseValue(value) {
        return new GeoPoint(value.latitude, value.longitude)
    },
    serialize(value) {
        return {latitude: value.latitude, longitude: value.longitude}
    }
})

const gqlWalker = {
    bool: new GraphQLNonNull(GraphQLBoolean),
    int: new GraphQLNonNull(GraphQLInt),
    float: new GraphQLNonNull(GraphQLFloat),
    str: new GraphQLNonNull(GraphQLString),
    datetime: new GraphQLNonNull(GraphQLDateTime),
    geopoint: new GraphQLNonNull(GraphQLGeoPoint),
    list: type => new GraphQLNonNull(new GraphQLList(type)),
    nullable: type => {
        while (type instanceof GraphQLNonNull) {
            type = type.ofType
        }
        return type;
    },
}

export function toGraphQLOutputType(typeDef: TypeDef, existingEntityTypes: { [key: string]: GraphQLOutputType } = {}): GraphQLOutputType {
    return walk(typeDef, (path) => ({
        ...gqlWalker,
        object: fields => {
            const name = pascalCase(path);
            return new GraphQLObjectType({
                name,
                fields: mapObject(fields, type => ({ 
                    type,
                })),
            } as any);
        },
        entity: (name, fields) => existingEntityTypes[name] = existingEntityTypes[name] || fields,
    }));
}

export function toGraphQLInputType(typeDef: TypeDef): GraphQLInputType {
    return walk(typeDef, path => ({
        ...gqlWalker,
        object: fields => {
            return new GraphQLInputObjectType({
                name: pascalCase(`${path} input`),
                fields: mapObject(fields, type => ({ type }))
            });
        },
        entity: () => new GraphQLNonNull(GraphQLID),
    }));
}

export const GraphQLEventType = new GraphQLEnumType({
    name: 'EventType',
    values: {
        created: { value: 'created' },
        updated: { value: 'updated' },
        deleted: { value: 'deleted' }
    }
})

export function toEventType(objectType: GraphQLObjectType): GraphQLObjectType {
    return new GraphQLObjectType({
        name: pascalCase(`${objectType.name} event`),
        fields: {
            id: { type: new GraphQLNonNull(GraphQLID) },
            event: { type: new GraphQLNonNull(GraphQLEventType) },
            value: { type: objectType }
        }
    })
}

export function toSchema({ queries, actions, subscriptions }: ProjectDef, dbHandle?: DBHandle): GraphQLSchema {
    const existingOutputTypes = {};
    const methodBagToGraphQLObjectType = (items: { [key: string]: MethodDef }, name: string,) => new GraphQLObjectType({
        name,
        fields: mapObject(items, ({ argumentTypes, returnType, fn }, name) => {
            const validateArgs = createValidator(argumentTypes);
            const validateReturnValue = createValidator(returnType);

            const resolve: GraphQLFieldResolver<any, any> | undefined = dbHandle && (async (obj, args, context, info) => { 
                const executionId = uniqid()
                try {
                    logger.info(`(${executionId}) Started executing ${name} with args ${JSON.stringify(args, undefined, 2)}`)
                    try {
                        validateArgs(args);
                    } catch(e) {
                        throw new UserInputError(e.message);
                    }
                    
                    const result = await fn(dbHandle, args);
                    validateReturnValue(result);
                    logger.info(`(${executionId}) Finished executing '${name}' with args ${JSON.stringify(args, undefined, 2)}`)
                    return result;   
                } catch(e) {
                    logger.info(`(${executionId}) Failed executing '${name}' with args ${JSON.stringify(args, undefined, 2)}: ${e.message}`)
                    throw e;
                }
            })

            return {
                args: mapObject(argumentTypes.fieldsTypes, typeDef => ({ type: toGraphQLInputType(typeDef) })),
                type: toGraphQLOutputType(returnType, existingOutputTypes),
                resolve
            }
        })
    })

    return new GraphQLSchema({
        query: methodBagToGraphQLObjectType(queries, 'Query'),
        mutation: actions && methodBagToGraphQLObjectType(actions, 'Mutation'),
        subscription: subscriptions && new GraphQLObjectType({
            name: 'Subscription',
            fields: mapObject(toObject(subscriptions, entity => entity.name), entity => {
                const subscribe: GraphQLFieldResolver<any, any> | undefined = dbHandle && (function (obj, args, context, {fieldName}) { 
                   return asyncify(async callback => dbHandle(entity).subscribe(x => callback({[fieldName]: x})), {onClose: unsub => unsub()})
                })

                return {
                    type: toEventType(toGraphQLOutputType(entity, existingOutputTypes) as GraphQLObjectType),
                    subscribe
                }
            })
        })
    })
}
#!/usr/bin/env node

import * as yargs from 'yargs';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { toSchema } from './gql';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { execute, GraphQLSchema, subscribe } from 'graphql';
import { createDataLoader, createDBHandle, EntityHandle } from './db';
import { Db, MongoClient } from 'mongodb';
import { createLogger } from './logger';
import {NodeVM} from 'vm2';
import * as path from 'path';
import cors from 'cors';
import download from 'download';
import tmp from 'tmp';
import rimraf from 'rimraf';
import isUri from 'is-uri';
import { assertProjectDef, entity, ProjectDef } from './defs';

export * from './defs';
export * from './gql';
export * from './db';
export * from './logger';

const logger = createLogger('global');

export interface Options { 
  mongo: string,
  db: string;
  port: number;
  project?: string,
  frontend?: string,
  allowedOrigins: string,
}

export const defaultOptions: Partial<Options> = {
  mongo: 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false',
  db: 'test',
  port: 4000,
  allowedOrigins: '*'
};

export async function run(opts: Partial<Options>) {
  const {project, mongo, db, port, frontend, allowedOrigins} = {...defaultOptions, ...opts} as Options

  const app = express();
  logger.info(`Allowed origins: ${allowedOrigins}`)
  app.use(cors({origin: allowedOrigins}))

  if(frontend) {
    logger.info(`Setting up SPA with frontend at '${frontend}'`)
    app.use('/',express.static(frontend))
    app.get('/', (req, res) => res.sendFile(path.join(frontend, 'index.html')))
  }

  let schema: GraphQLSchema;
  let apolloServer: ApolloServer;
  if(project) {
    let projectFile = project;
    let tmpDir;
    if(project.startsWith('http')) {
      logger.info('Downloading project...');
      const {name} = tmp.dirSync();
      tmpDir = name;
      const filename = 'project.js';
      await download(project, tmpDir, {filename})
      projectFile = path.join(tmpDir, filename);
    }
    
    const vm = new NodeVM({sandbox: module.exports});
    
    logger.info(`Reading project definition as ${isUri(projectFile) ? 'file' : 'source'}`)
    const projectDef: ProjectDef = assertProjectDef(isUri(projectFile) ? vm.runFile(projectFile) : vm.run(projectFile));

    if(tmpDir) {
      rimraf.sync(tmpDir)
    }

    logger.info(`Fetched project definition: `);
    logger.info(`\tQueries: ${Object.keys(projectDef.queries).join(', ')}`)
    logger.info(`\tActions: ${projectDef.actions && Object.keys(projectDef.actions).join(', ') || 'None'}`)
    logger.info(`\tSubscriptions: ${projectDef.subscriptions && projectDef.subscriptions.map(entityDef => entityDef.name).join(', ') || 'None'}`);
  
    logger.info(`Connecting to MongoDB db='${db}' at ${mongo}...`)
    const client = await MongoClient.connect(mongo, {useUnifiedTopology: true})
    logger.info(`Successfully connected to MongoDB`)
    const dbHandle = createDBHandle(client.db(db));
    const dataloader = createDataLoader(dbHandle);
    
    schema = toSchema(projectDef, dbHandle)

    apolloServer = new ApolloServer({
      schema, 
      context: {dataloader}
    });
    apolloServer.applyMiddleware({app});
  }
  
  const server = createServer(app);

  server.listen(port, () => {
    if(project) {
      new SubscriptionServer({
        execute,
        subscribe,
        schema,
      }, {
        server,
        path: apolloServer.graphqlPath,
      });
      logger.info(`GraphQL Subscriptions:\tws://localhost:${port}${apolloServer.graphqlPath}`)
      logger.info(`GraphQL API Endpoint:\thttp://localhost:${port}${apolloServer.graphqlPath}`)
    }

    if(frontend) {
      logger.info(`Frontend Endpoint:\thttp://localhost:${port}/`)
    }
    logger.info(`Server running...`)
  })
}

if (require.main === module) {
  require('dotenv').config();
  const argv = yargs.option('mongo', {
    alias: 'm',
    desc: 'MongoDB connection string',
    type: 'string'
  }).option('db', {
    desc: 'MongoDB database name',
    type: 'string',
    default: defaultOptions.db
  }).option('port', {
    desc: 'Port to listen on',
    type: 'number',
    default: defaultOptions.port
  }).option('project', {
    alias: 'p',
    desc: 'A feather project path',
    type: 'string'
  }).option('frontend', {
    alias: 'f',
    desc: 'Path to directory containing index.html',
    type: 'string'
  }).option('allowedOrigins', {
    alias: 'o',
    desc: 'Allowed origins header',
    type: 'string',
    default: defaultOptions.allowedOrigins
  }).argv;

  run(argv);
}
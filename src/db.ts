import { Collection, Db, FilterQuery, MongoClient } from 'mongodb';
import { createValidator, EntityDef, GeoPoint, walk, walkerWithDefault } from './defs';
import uniqid from 'uniqid';
import { groupBy, mapObject, sleep, toObject } from './utils';
import x from 'uniqid';
import DataLoader from 'dataloader';
import { createLogger } from './logger';

const logger = createLogger('database');

export type DBHandle = (ent: EntityDef) => EntityHandle;

export function createDBHandle(db: Db): DBHandle {
    return (ent: EntityDef) => new EntityHandle(db, ent);
} 

export interface Instance {
    id: string,
    created: Date,
    updated: Date,
    [key: string]: any;
}

export enum EventType {
    created = 'created',
    updated = 'updated',
    deleted = 'deleted'
}

export interface Event {
    id: string,
    event: EventType,
    value: Instance;
}

export class EntityHandle {
    collection: Collection;
    validate: (x: Instance) => void;
    fromDB: (x: any) => any;
    toDB: (x: any) => any;
    constructor(db: Db, private entityDef: EntityDef) {
        this.collection = db.collection(entityDef.name);

        this.validate = createValidator(entityDef);
        this.fromDB = walk(entityDef, walkerWithDefault(x => x, path => ({
            geopoint: ({coordinates}) => new GeoPoint(coordinates[1], coordinates[0]),
            list: converter => x => x.map(converter),
            nullable: converter => x => x && converter(x),
            object: fieldConverters => x => mapObject(x, (v, k) => fieldConverters[k](v)),
            entity: (name, fieldsConverter) => ({_id, ...rest}) => ({id: _id, ...fieldsConverter({id: _id, ...rest})})
        })))
        this.toDB = walk(entityDef, walkerWithDefault(x => x, path => ({
            geopoint: (x: GeoPoint) => ({ type: "Point", coordinates: [ x.longitude, x.latitude ] }),
            list: converter => x => x.map(converter),
            nullable: converter => x => x ? converter(x) : null,
            object: fieldConverters => x => mapObject(x, (v, k) => fieldConverters[k](v)),
            entity: (name, fieldsConverter) => ({id, ...rest}) => ({_id: id, ...fieldsConverter({id, ...rest})})
        })))
    }

    async set(id?: string, values?: any): Promise<Instance> {
        if(!id && !values) {
            throw new Error(`Please provided at least id or values to the 'set' method`);
        }

        if(!id && this.entityDef.deriveId) {
            id = this.entityDef.deriveId(values);
        }

        if(!id) {
            id = uniqid();
        }

        id = `${id}`; 

        if(values) {
            const now = new Date();
            const extendedValues = {...values, id, created: now, updated: now};
            this.validate(extendedValues);
            const {id: _, created: __, updated: ___, ...writtenValues} = this.toDB(extendedValues);
            const {value} = await this.collection.findOneAndUpdate({_id: id}, {
                $set: {...writtenValues, _id: id, updated: now}, 
                $setOnInsert: {created: now}
            }, {upsert: true, returnOriginal: false});
            const result = this.fromDB(value);
            if(result.created.getTime() === result.updated.getTime()) {
                logger.info(`Created '${this.entityDef.name}': ${JSON.stringify(result, undefined, 2)}`)
            } else {
                logger.info(`Updated '${this.entityDef.name}': ${JSON.stringify(result, undefined, 2)}`)
            }
            return result;
        } else {
            const {value} = await this.collection.findOneAndDelete({_id: id});
            const result = this.fromDB(value);
            logger.info(`Deleted '${this.entityDef.name}': ${JSON.stringify(result, undefined, 2)}`)
            return result;
        }
    }

    async create(values: any): Promise<Instance> {
        return this.set(undefined, values);
    }

    async update(id: string, values: any): Promise<Instance> {
        return this.set(id, values);
    }

    async delete(id: string): Promise<Instance> {
        return this.set(id);
    }

    async byId(id: string): Promise<Instance> {
        const result = await this.collection.findOne({id})
        const out = result && this.fromDB(result)
        logger.info(`Queried '${this.entityDef.name}' by id ${id}: ${JSON.stringify(out, undefined, 2)}`)
        return out;
    }

    async byIds(ids: string[]): Promise<Instance[]> {
        const results = await this.collection.find({id: {$in: ids}}).toArray()
        const out = results.map(this.fromDB);
        logger.info(`Queried '${this.entityDef.name}' by ${ids.length} ids`)
        return out;
    }

    async find(filter: FilterQuery<any>): Promise<Instance[]> {
        const results = await this.collection.find(filter).toArray();
        const out = results.map(this.fromDB)
        logger.info(`Queried '${this.entityDef.name}' by filter: ${JSON.stringify(filter, undefined, 2)}, got ${out.length} results`)
        return out;
    }

    async count(filter: FilterQuery<any>): Promise<number> {
        const count = await this.collection.countDocuments(filter)
        logger.info(`Queried '${this.entityDef.name}' for count, got ${count}`)
        return count;
    }

    subscriptionCount = 0;
    subscribe(onEvent: (event: Event) => void): () => void {
        const stream = this.collection.watch({ fullDocument: 'updateLookup' });
        logger.info(`Subscribed to '${this.entityDef.name}', number of active subscriptions: ${++this.subscriptionCount}`)
        stream.on('change', (doc: any) => {
            const value = this.fromDB(doc.fullDocument);
            const event = {
                insert: EventType.created, 
                update: EventType.updated, 
                delete: EventType.deleted
            }[doc.operationType];
            onEvent({id: value.id, event, value})
        })
        return async () => {
            await stream.close();
            logger.info(`Unsubscribed from '${this.entityDef.name}, number of active subscriptions: ${--this.subscriptionCount}'`)
        };
    }
}

export interface DataLoaderKey {
    entityDef: EntityDef,
    id: string
}

export function createDataLoader(dbHandle: DBHandle): DataLoader<DataLoaderKey, Instance> {
    return new DataLoader(async keys => {
        const entitiesByName = toObject(keys.map(x => x.entityDef), x => x.name);
        const groups = groupBy(keys, x => x.entityDef.name);
        const map = {}
        for(const [entityName, keys] of Object.entries(groups)) {
            const instances = await dbHandle(entitiesByName[entityName]).byIds(keys.map(k => k.id));
            for(let i = 0; i < keys.length; i++) {
                const id = keys[i].id
                map[entityName] = map[entityName] || {}
                map[entityName][id] = instances[i];
            }
        }
        return keys.map(key => map[key.entityDef.name][key.id]);
    })
}
import { TypeDef, walk, Walker, bool, int, float, str, list, nullable, object, entity, ObjectDef, EntityDef, ProjectDef, MethodDef, method, datetime, WalkerFn, geopoint } from "./defs";


export const Child: EntityDef = entity({
    name: 'Child',
    fields: object({
        name: str
    })
})

// just a very complicated type def for testing purposes
export const complicatedEntity: EntityDef = entity({
    name: 'Person',
    fields: object({
        name: str,
        birthday: datetime,
        favChild: Child,
        children: list(Child),
        age: nullable(int),
        location: geopoint,
        married: bool,
        salary: float,
        addresses: list(object({
            street: str,
            number: int
        }))
    })
})

export const complicatedMethod: MethodDef = method(
    object({foo: int, bar: nullable(str)}), 
    list(complicatedEntity), 
    async () => {}
)

export const complicatedProjectDef: ProjectDef = {
    queries: {
        someQuery: complicatedMethod
    },
    actions: {
        someAction: complicatedMethod
    },
    subscriptions: [complicatedEntity]
}

// walker that simply returns the same TypeDef that it got
export const trivialWalker: WalkerFn<TypeDef> = path => ({
    bool,
    int,
    float,
    str,
    datetime,
    geopoint,
    list,
    nullable,
    object,
    entity: (name, fields) => entity({name, fields: fields as ObjectDef})
});

describe('AST definitions', () => {
    it('Walk traverses AST correctly', () => {
        expect(walk(complicatedEntity, trivialWalker)).toEqual(complicatedEntity)
    })

    it('TypeDefs can be serialized and deserialized to/from json', () => {
        expect(JSON.parse(JSON.stringify(complicatedEntity))).toEqual(complicatedEntity);
    })
})
import { assert, mapObject } from "./utils";
import { camelCase, dotCase } from 'change-case';
import { DBHandle } from "./db";

export interface BoolDef { type: 'bool' }
export function isBoolDef(x: any): x is BoolDef { return x.type === 'bool' }
export const bool: BoolDef = { type: 'bool' };

export interface IntDef { type: 'int' }
export function isIntDef(x: any): x is IntDef { return x.type === 'int' }
export const int: IntDef = { type: 'int' };

export interface FloatDef { type: 'float' }
export function isFloatDef(x: any): x is FloatDef { return x.type === 'float' }
export const float: FloatDef = { type: 'float' }

export interface StrDef { type: 'str' }
export function isStrDef(x: any): x is StrDef { return x.type === 'str' }
export const str: StrDef = { type: 'str' };

export interface DateTimeDef { type: 'datetime' }
export function isDateTimeDef(x: any): x is DateTimeDef { return x.type === 'datetime' }
export const datetime: DateTimeDef = { type: 'datetime' };

export interface GeoPointDef { type: 'geopoint' }
export function isGeoPointDef(x: any): x is GeoPointDef { return x.type === 'geopoint' }
export const geopoint: GeoPointDef = { type: 'geopoint' }

export class GeoPoint {
    constructor(public latitude: number, public longitude) {};
}


export type ScalarDef = BoolDef | IntDef | FloatDef | StrDef | DateTimeDef | GeoPointDef;
export function isScalarDef(x: any): x is ScalarDef { return isBoolDef(x) || isIntDef(x) || isFloatDef(x) || isStrDef(x) || isDateTimeDef(x) || isGeoPointDef(x) }


export interface ListDef { type: 'list', ofType: TypeDef };
export function isListDef(x: any): x is ListDef { return x.type === 'list' };
export function list(ofType: TypeDef): ListDef { return { type: 'list', ofType: assertTypeDef(ofType) } }

export interface NullableDef { type: 'nullable', ofType: TypeDef };
export function isNullableDef(x: any): x is NullableDef { return x.type === 'nullable' };
export function nullable(ofType: TypeDef): NullableDef { return { type: 'nullable', ofType: assertTypeDef(ofType) } }

export interface ObjectDef { type: 'object', fieldsTypes: { [key: string]: TypeDef } }
export function isObjectDef(x: any): x is ObjectDef { return x.type === 'object' };
export function object(fieldTypes: { [key: string]: TypeDef }): ObjectDef { return { type: 'object', fieldsTypes: mapObject(fieldTypes, assertTypeDef) } }

export function partial(obj: ObjectDef): ObjectDef {
    return object(mapObject(obj.fieldsTypes, nullable));
}

export interface EntityDef {
    type: 'entity';
    name: string,
    fields: ObjectDef,
    validate?: (instance: any) => void,
    deriveId?: (instance: any) => string,
}
export function isEntityDef(x: any): x is EntityDef { return x.type === 'entity' }

export function entity({ name, fields, validate, deriveId }: Omit<EntityDef, 'type'>): EntityDef {
    return {
        type: 'entity',
        name,
        fields: object({ ...fields.fieldsTypes, id: str, created: datetime, updated: datetime }),
        validate,
        deriveId
    };
}

export function defaultQueries(entity: EntityDef) {
    return {
        [camelCase(`${entity.name} by id`)]: method(object({ id: str }), nullable(entity), (handle, { id }) => handle(entity).byId(id)),
        [camelCase(`${entity.name} by ids`)]: method(object({ ids: list(str) }), list(nullable(entity)), (handle, { ids }) => handle(entity).byIds(ids)),
        [camelCase(`find ${entity.name}s`)]: method(partial(entity.fields), list(nullable(entity)), (handle, args) => handle(entity).find(args)),
        [camelCase(`${entity.name} count`)]: method(partial(entity.fields), int, (handle, args) => handle(entity).count(args))
    }
}

export function defaultActions(entity: EntityDef) {
    const { id, created, updated, ...rawFields } = entity.fields.fieldsTypes;
    return {
        [camelCase(`create ${entity.name}`)]: method(object(rawFields), entity, (handle, args) => handle(entity).create(args)),
        [camelCase(`update ${entity.name}`)]: method(object({ id: str, ...rawFields }), entity, (handle, { id, ...args }) => handle(entity).update(id, args)),
        [camelCase(`delete ${entity.name}`)]: method(object({ id: str }), entity, (handle, { id }) => handle(entity).delete(id))
    }
}

export type TypeDef = ScalarDef | ListDef | NullableDef | ObjectDef | EntityDef;
export function isTypeDef(x: any): x is TypeDef { return isScalarDef(x) || isListDef(x) || isObjectDef(x) || isNullableDef(x) || isEntityDef(x) }
export function assertTypeDef(x: any, name?: string): TypeDef {
    if (!isTypeDef(x)) {
        throw new Error(`Expected a TypeDef${name && ` for ${name}`}, got ${JSON.stringify(x)}`)
    }
    return x;
}

export interface MethodDef {
    type: 'method',
    argumentTypes: ObjectDef,
    returnType: TypeDef,
    fn: (handle: DBHandle, args: any) => Promise<any>
}
export function isMethodDef(x: any): x is MethodDef { return x.type === 'method' }
export function method(argumentTypes: ObjectDef, returnType: TypeDef, fn: (handle: DBHandle, args: any) => Promise<any>): MethodDef {
    return {
        type: 'method',
        argumentTypes,
        returnType,
        fn
    }
}

export interface Walker<A> {
    bool: A,
    int: A,
    float: A,
    str: A,
    datetime: A,
    geopoint: A,
    list: (a: A) => A,
    nullable: (a: A) => A
    object: (fields: { [key: string]: A }) => A,
    entity: (name: string, fields: A, validate: ((x: any) => void) | undefined, deriveId: ((x: any) => string) | undefined) => A
}

export type WalkerFn<A> = (path: string, breadCrumbs: TypeDef[]) => Walker<A>

export function walkerWithDefault<A>(defaultValue: A, partialWalkerFn: (path: string) => Partial<Walker<A>>): WalkerFn<A> {
    return path => {
        return {
            bool: defaultValue,
            int: defaultValue,
            float: defaultValue,
            str: defaultValue,
            datetime: defaultValue,
            geopoint: defaultValue,
            list: () => defaultValue,
            nullable: () => defaultValue,
            object: () => defaultValue,
            entity: () => defaultValue,
            ...partialWalkerFn(path)
        }
    }
}


export function walk<A>(typeDef: TypeDef, walkerFn: WalkerFn<A>, path?: string, breadCrumbs?: TypeDef[]): A {
    const newBreadCrumbs = [typeDef, ...(breadCrumbs || [])]
    const walker = walkerFn(path || '', newBreadCrumbs);
    if (isScalarDef(typeDef)) {
        if (isBoolDef(typeDef)) {
            return walker.bool
        }
        if (isIntDef(typeDef)) {
            return walker.int
        }
        if (isFloatDef(typeDef)) {
            return walker.float
        }
        if (isStrDef(typeDef)) {
            return walker.str
        }
        if (isDateTimeDef(typeDef)) {
            return walker.datetime
        }
        if (isGeoPointDef(typeDef)) {
            return walker.geopoint
        }
    }

    if (isListDef(typeDef)) {
        return walker.list(walk(typeDef.ofType, walkerFn, path, newBreadCrumbs))
    }

    if (isNullableDef(typeDef)) {
        return walker.nullable(walk(typeDef.ofType, walkerFn, path, newBreadCrumbs))
    }

    if (isObjectDef(typeDef)) {
        return walker.object(mapObject(typeDef.fieldsTypes, (def, key) => walk(def, walkerFn, `${path} ${key}`, newBreadCrumbs)))
    }

    if (isEntityDef(typeDef)) {
        return walker.entity(typeDef.name, walk(typeDef.fields, walkerFn, typeDef.name, newBreadCrumbs), typeDef.validate, typeDef.deriveId);
    }

    throw new Error(`Unknown type def ${JSON.stringify(typeDef)}`)
}

export interface ProjectDef {
    queries: { [name: string]: MethodDef },
    actions?: { [name: string]: MethodDef },
    subscriptions?: EntityDef[]
}

export function assertProjectDef(x: any): ProjectDef {
    const {queries, actions, subscriptions} = x;
    try {
        assert(typeof queries === 'object', 'Queries must be an object', 'queries');
        assert(Object.keys(queries).length > 0, 'Must define at least one query')
        mapObject(queries, (value, field) => assert(isMethodDef(value), 'Query must be a method definition', `queries.${field}`))
        assert(!actions || typeof actions === 'object', 'Actions must be an object or undefined', 'actions');
        actions && mapObject(actions, (value, field) => assert(isMethodDef(value), 'Action must be a method definition', `actions.${field}`))
        assert(!subscriptions || typeof subscriptions === 'object', 'Subscriptions must be an array or undefined', 'subscriptions');
        subscriptions && subscriptions.map((value, i) => assert(isEntityDef(value), 'Subscription must be an entity definition', `subscriptions.[${i}]`))
    } catch(e) {
        throw new Error(`Invalid Project Definition: ${e.message}`)
    }
    
    return x;
}

export const createValidator = (typeDef: TypeDef) => walk<(x: any, originalValue?: any) => void>(typeDef, path => ({
    bool: (x, original) => assert(typeof x === 'boolean', `Invalid bool value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    int: (x, original) => assert(Number.isFinite(x) && Number.isInteger(x), `Invalid int value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    float: (x, original) => assert(Number.isFinite(x), `Invalid float value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    str: (x, original) => assert(typeof x === 'string', `Invalid str value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    datetime: (x, original) => assert(x instanceof Date, `Invalid datetime value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    geopoint: (x, original) => assert(x instanceof GeoPoint, `Invalid geopoint value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path),
    list: validate => (x, original) => {
        assert(Array.isArray(x), `Invalid list value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path);
        for (const value of x) {
            validate(value, original || x);
        }
    },
    nullable: validate => (x, original) => x && validate(x, original || x),
    object: fieldValidators => (x, original) => {
        assert(typeof x === 'object', `Invalid object value ${JSON.stringify(x)} in ${JSON.stringify(original)}`, path);
        for (const [k, v] of Object.entries(x)) {
            fieldValidators[k](v, original || x);
        }
    },
    entity: (name, validate, entityValidate) => (x, original) => {
        validate(x, original || x);
        try {
            entityValidate && entityValidate(x);
        } catch (e) {
            throw new Error(`${name} Entity validation in ${JSON.stringify(original)} failed with: ${e.message}`);
        }
    }
}))
import { gql } from 'apollo-server-express';
import { GraphQLNamedType, GraphQLOutputType, GraphQLSchema, printSchema, typeFromAST } from 'graphql';
import { complicatedEntity, complicatedProjectDef } from './defs.spec';
import {toGraphQLInputType, toGraphQLOutputType, toSchema} from './gql';


describe('GraphQL', () => {
    it('toGraphQLOutputType complicated example', () => {
        const existingOutputTypes: {[key: string]: GraphQLOutputType} = {};
        const type = toGraphQLOutputType(complicatedEntity, existingOutputTypes);

        const schema = new GraphQLSchema({
            types: [type as GraphQLNamedType]
        })

        expect(printSchema(schema)).toMatchSnapshot()
        expect(existingOutputTypes['Person']).toEqual(type)
        // TODO: check PersonAddresses
    })

    it('toGraphQLInputType complicated example', () => {
        const type = toGraphQLInputType(complicatedEntity);

        const schema = new GraphQLSchema({
            types: [type as GraphQLNamedType]
        })

        expect(printSchema(schema)).toMatchSnapshot()
    })

    it('toSchema happy case', () => {
        const schema = toSchema(complicatedProjectDef);

        expect(printSchema(schema)).toMatchSnapshot();
    })

    it('toSchema no actions no subscriptions', () => {
        const schema = toSchema({queries: complicatedProjectDef.queries});

        expect(printSchema(schema)).toMatchSnapshot();
    })
})
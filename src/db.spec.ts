import { Db, MongoClient } from 'mongodb';
import { createDBHandle, DBHandle } from './db';
import { consoleTransport } from './logger';
import { entity, int, object, str } from './defs';
import { sleep } from './utils';
 
consoleTransport.silent = true;

// Connection URL
const url = process.env.MONGO_URL || 'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false';

const Person = entity({
    name: 'Person',
    fields: object({
        name: str,
        age: int,
    })
})


const PersonDeriveId = entity({
    name: 'Person',
    fields: object({
        name: str,
        age: int,
    }),
    deriveId({name, age}) {
        return `${name}-${age}`
    }
})

describe('DB', () => {

    let client: MongoClient;
    let db: Db;
    let dbHandle: DBHandle;
    beforeAll(async () => {
        client = await MongoClient.connect(url, {useUnifiedTopology: true});
        db = client.db('test');
        dbHandle = createDBHandle(db);
    })

    beforeEach(async () => {
        await db.dropDatabase();
    })

    afterAll(async () => {
        await db.dropDatabase();
        await client.close(true)
    })

    it('create works', async () => {
        const personHandle = dbHandle(Person);

        const values = {name: 'Foo', age: 23}
        const result = await personHandle.create(values);

        const dbFetch = await db.collection('Person').findOne({_id: result.id});
        expect(dbFetch).toMatchObject({_id: result.id, ...values})
        expect(dbFetch).toEqual({...result, _id: result.id, id: undefined})
        expect(dbFetch).toHaveProperty('created')
        expect(dbFetch).toHaveProperty('updated')
        expect(dbFetch.created).toEqual(dbFetch.updated);
    })

    it('create works (deriveId)', async () => {
        const personHandle = dbHandle(PersonDeriveId);

        const values = {name: 'Foo', age: 23}
        const result = await personHandle.create(values);
        const id = PersonDeriveId.deriveId!(values);

        const dbFetch = await db.collection('Person').findOne({_id: id});
        expect(dbFetch).toMatchObject({_id: id, ...values})
        expect(dbFetch).toEqual({...result, _id: id, id: undefined})
        expect(dbFetch).toHaveProperty('created')
        expect(dbFetch).toHaveProperty('updated')
        expect(dbFetch.created).toEqual(dbFetch.updated);
    })

    it('set works', async () => {
        const personHandle = dbHandle(Person);

        const id = 'asdf';
        const values = {name: 'Foo', age: 23}
        const result = await personHandle.set(id, values);
        expect(result.id).toEqual(id)

        const dbFetch = await db.collection('Person').findOne({_id: id});
        expect(dbFetch).toMatchObject({_id: id, ...values})
        expect(dbFetch).toEqual({...result, _id: id, id: undefined})
        expect(dbFetch).toHaveProperty('created')
        expect(dbFetch).toHaveProperty('updated')
        expect(dbFetch.created).toEqual(dbFetch.updated);
    })

    it('update works', async () => {
        const personHandle = dbHandle(Person);

        const values = {name: 'Foo', age: 23}
        const creationResult = await personHandle.create(values);
        const id = creationResult.id;

        await sleep(400);

        const updatedResult = await personHandle.update(id, {name: 'Bar'});

        const {updated, ...withoutUpdated} = creationResult;
        expect(updatedResult).toMatchObject({...withoutUpdated, name: 'Bar',});
        expect(updatedResult.updated.getTime()).toBeGreaterThan(updatedResult.created.getTime());

        const dbFetch = await db.collection('Person').findOne({_id: id});
        expect(dbFetch).toMatchObject({_id: id, ...values, name: 'Bar'})
        expect(dbFetch).toEqual({...updatedResult, _id: id, id: undefined})
        expect(dbFetch.created).toEqual(updatedResult.created)
        expect(dbFetch.updated).toEqual(updatedResult.updated)
    })

    it('delete works', async () => {
        const personHandle = dbHandle(Person);

        const values = {name: 'Foo', age: 23}
        const creationResult = await personHandle.create(values);
        const id = creationResult.id;

        await sleep(400);

        const deleteResult = await personHandle.delete(id);

        expect(deleteResult).toEqual(creationResult);

        const dbFetch = await db.collection('Person').findOne({_id: id});
        expect(dbFetch).toEqual(null)
    })

    it('validation works', async () => {
        const personHandle = dbHandle(Person);

        const values = {name: 'Foo', age: 'hello'}
        await expect(personHandle.create(values)).rejects.toThrowError('Invalid')
    })
})
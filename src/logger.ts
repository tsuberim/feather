import * as winston from 'winston';
import chalk from 'chalk';

export const consoleTransport = new winston.transports.Console({
    format: winston.format.printf(({timestamp, level, message, name}) => {
        const color = {
            info: 'blue', 
            debug: 'gray', 
            error: 'red', 
            warn: 'yellow',
            verbose: 'magenta',
            silly: 'green',
            http: 'green'
        }[level];
        return chalk`{grey [${timestamp}]} {black.underline [${name}]}\t {${color}.bold [${level}]}:\t${message}`;
    })
})

export function createLogger(name: string) {
    return winston.createLogger({
        defaultMeta: {name},
        format: winston.format.timestamp(),
        transports: [
            consoleTransport
        ]
    })
}

export const logger = createLogger('service')